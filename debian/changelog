python-apptools (4.5.0-2) UNRELEASED; urgency=medium

  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Thu, 24 Sep 2020 08:55:21 +0200

python-apptools (4.5.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * [5479ab1] Fix test against python3.9. (Closes: #973074)

 -- Anton Gladky <gladk@debian.org>  Mon, 30 Nov 2020 21:59:21 +0100

python-apptools (4.5.0-1) unstable; urgency=medium

  * Team upload.
  * Update to new upstream release 4.5.0
  * Remove upstreamed/unneeded patches & enable tests
  * Fix StopIteration exception observed on Python 3.7+
  * Fix SyntaxWarning in file_path.py
  * Fix spelling error in debian/patches/dont_delete_tmp
  * Fix path in d/python-apptools-doc.doc-base file

 -- Scott Talbert <swt@techie.net>  Thu, 02 Jan 2020 11:23:31 -0500

python-apptools (4.4.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one.
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #943923, #937577, #935202

 -- Sandro Tosi <morph@debian.org>  Sun, 15 Dec 2019 16:08:24 -0500

python-apptools (4.4.0-3) unstable; urgency=medium

  * Fixing broken links in python-apptools-doc

 -- Stewart Ferguson <stew@ferg.aero>  Tue, 05 Mar 2019 21:26:46 +0100

python-apptools (4.4.0-2) unstable; urgency=medium

  * Adding Breaks and Replaces to new doc package (Closes: #922286)

 -- Stewart Ferguson <stew@ferg.aero>  Thu, 14 Feb 2019 21:19:20 +0100

python-apptools (4.4.0-1) unstable; urgency=medium

  [ SVN-Git Migration ]
  * svn-git migration

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout

  [ Stewart Ferguson ]
  * New upstream release
  * Adding myself to Uploaders
  * Migrating from cdbs to pybuild
  * Releasing HTML documentation
  * Releasing man pages
    - Added patch to release man pages under section 3.
  * Bumping compat to 12
  * Bump Standards-Version to 4.3.0
  * Now using python3-sphinx to build docs
    - Added patch support_python3-sphinx to achieve this
  * Fixing FTBFS cleanup issue (Closes: #920293, #671216).
  * Releasing python3 version
  * Releasing doc binary package
  * Changing traitsui from Depends to Recommends (in d/control)
    - patching to make work with no-display minimal containers
      with root privileges (see patch removing_traitsui)
  * Replacing old d/compat with debhelper-compat build dependency
  * Dropping version restriction on python-all
  * Adding patch dont_delete_tmp to allow building in chroot
  * Adding patch lintian_spelling to address some spelling mistakes
  * Adding patch skip_test_file_properties to skip unit test which
    (understantably) fail when run as root
  * Removing unused d/orig-tar.sh
  * Updating d/changelog to dep5 format, added new copyright holders,
    removed deleted icon licenses.

 -- Stewart Ferguson <stew@ferg.aero>  Wed, 06 Feb 2019 19:00:44 +0100

python-apptools (4.3.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 3.9.6

 -- Varun Hiremath <varun@debian.org>  Mon, 10 Aug 2015 00:28:51 -0400

python-apptools (4.2.1-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Varun Hiremath ]
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Varun Hiremath <varun@debian.org>  Sat, 15 Mar 2014 22:47:59 -0400

python-apptools (4.0.1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3

 -- Varun Hiremath <varun@debian.org>  Mon, 23 Apr 2012 22:36:25 -0400

python-apptools (4.0.0-1) unstable; urgency=low

  * New upstream release
  * Update debian/watch file

 -- Varun Hiremath <varun@debian.org>  Fri, 08 Jul 2011 23:55:50 -0400

python-apptools (3.4.1-3) unstable; urgency=low

  * Team upload.
  * Rebuild to pick up 2.7 and drop 2.5 from supported versions
  * d/control: Bump Standards-Version to 3.9.2

 -- Sandro Tosi <morph@debian.org>  Sat, 07 May 2011 09:59:41 +0200

python-apptools (3.4.1-2) unstable; urgency=low

  * d/rules: fix pyshared directory path

 -- Varun Hiremath <varun@debian.org>  Wed, 06 Apr 2011 19:43:52 -0400

python-apptools (3.4.1-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #616990)

 -- Varun Hiremath <varun@debian.org>  Tue, 05 Apr 2011 21:25:31 -0400

python-apptools (3.4.0-1) experimental; urgency=low

  * New upstream release
  * d/control: Bump Standards-Version to 3.9.1
  * d/copyright: Fix BSD license text

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 14:15:32 -0400

python-apptools (3.3.2-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 03 Jun 2010 01:42:16 -0400

python-apptools (3.3.1-1) unstable; urgency=low

  * New upstream release
  * Switch to source format 3.0
  * Bump Standards-Version to 3.8.4
  * Bump dh compat to 7

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 13:59:02 -0500

python-apptools (3.3.0-3) unstable; urgency=low

  * debian/control: drop python-traitsgui from Depends to break
    circular dependency	(Closes: #561139)

 -- Varun Hiremath <varun@debian.org>  Tue, 15 Dec 2009 10:44:08 -0500

python-apptools (3.3.0-2) unstable; urgency=low

  * debian/control
    - Add libjs-query to Depends (Closes: #560722)
    - Add python-configobj to Depends (Closes: #560725)
    - Bump Standards-Version to 3.8.3

 -- Varun Hiremath <varun@debian.org>  Sat, 12 Dec 2009 23:47:08 -0500

python-apptools (3.3.0-1) unstable; urgency=low

  * New upstream release
  * Fix lintian warnings in debian/copyright
  * Bump Standards-Version to 3.8.2
  * Remove embedded jquery files and create links to files provided in
    libjs-jquery package

 -- Varun Hiremath <varun@debian.org>  Fri, 17 Jul 2009 19:08:17 -0400

python-apptools (3.2.0-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 26 Mar 2009 20:22:47 -0400

python-apptools (3.1.0-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control:
    + add python-setupdocs to Build-Depends
    + set XS-Python-Version: current

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:29:25 -0500

python-apptools (3.0.0-1) experimental; urgency=low

  * Initial release

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 00:58:08 -0400
